from launch import LaunchDescription
from launch_ros.actions import Node

def generate_launch_description():
    return LaunchDescription([
        Node(
            package='basic_sprayer',
            executable='mpu6050'
        ),
        Node(
            package='basic_sprayer',
            executable='driver'
        ),
        Node(
            package='basic_sprayer',
            executable='crsf_joysticks'
        ),
        Node(
            package='basic_sprayer',
            executable='kinematics'
        ),
        Node(
            package='basic_sprayer',
            executable='angleadc'
        ),
         Node(
            package='basic_sprayer',
             executable='dgps'
         ),
        # Node(
        #     package='basic_sprayer',
        #     executable='gps_subscriber'
        # ),
        # Node(
        #     package='basic_sprayer',
        #     executable='ecef'
        # )
    ])

