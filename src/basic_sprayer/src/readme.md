

# 调参指令
ros2 topic pub --once /kinepid basic_sprayer_interfaces/msg/Pid "{p: 0.06,d: 0.001}"

运行

ros2 launch basic_sprayer basic_sprayer_launch.py


编译

colcon build --packages-select basic_sprayer

# driver_speed.zndp1

驱动器配置文件


# kinematics1.cpp

这个文件是一开始的速度闭环的文件  控制频率 在50Hz 左右

# kinematics1.cpp

同样使用速度闭环,控制频率提升至100Hz，在每次读到转角的时候进行控制


# recode.cpp  记录下数据 用作训练

打开多个文件  分别记录

每次根据时间新建一个文件夹,然后将数据写入

- 需要保存的参数

- HUMANCMD

- MPU6050

- 四个车轮的目标速度

- 四个车轮的返回数据

- 前后转向桥的转角

KERNELS=="1-1.3:1.0", MODE:="0777", SYMLINK+="usb_0"

KERNELS=="1-1.4:1.0", MODE:="0777", SYMLINK+="usb_1"

KERNELS=="1-1.1:1.0", MODE:="0777", SYMLINK+="usb_2"

KERNELS=="1-1.2:1.0", MODE:="0777", SYMLINK+="usb_3"


