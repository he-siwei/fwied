/*
*********************************************************************************************************
*
*	ģ������ : GPS��λģ����������
*	�ļ����� : bsp_g_tGPS.c
*	��    �� : V1.1
*	˵    �� : ����GPSЭ�� NMEA-0183Э��, ������ԭ��
*
*	�޸ļ�¼ :
*		�汾��  ����        ����     ˵��
*		V1.0    2013-02-01 armfly  ��ʽ����
*		V1.1    2014-02-04 armfly  ����ȫ�ֱ�־��ʾ GPSģ�鴮������
*
*	Copyright (C), 2013-2014, ���������� www.armfly.com
*
*********************************************************************************************************
*/
#include <stdint.h>
#include <stdio.h>
#include "bsp_gps.h"
#include <string.h>
#include <stdlib.h>

/* ������������꣬��ʾ��GPSģ�������ת����COM1�ڣ�����ͨ�����Թ۲�ԭʼ���� */
//#define DEBUG_GPS_TO_COM1
#define TRUE 1
#define FALSE 0

void Analyze0183(uint8_t *_ucaBuf, uint16_t _usLen);
int32_t StrToInt(char *_pStr);
int32_t StrToIntFix(char *_pStr, uint8_t _ucLen);
void HexToAscii(uint8_t *_ucpHex, uint8_t *_ucpAscII, uint8_t _ucLenasc);

GPS_T g_tGPS;

/*
*********************************************************************************************************
*	�� �� ��: bsp_InitGPS
*	����˵��: ����GPS����
*	��    ��:  ��
*	�� �� ֵ: ��
*********************************************************************************************************
*/
void bsp_InitGPS(void)
{
	/*
		GPS ģ��ʹ�� UART ���ڷ��Ͷ�λ������ STM32, ÿ���ӷ���һ������

		��ˣ�ֻ��Ҫ���ô��ڼ��ɡ� bsp_uart_fifo.c ���Ѿ����úô��ڣ��˴�����������
	*/

	g_tGPS.PositionOk = 0;  /* ��������Ϊ��Ч */
	g_tGPS.TimeOk = 0;      /* ��������Ϊ��Ч */
	
	g_tGPS.UartOk = 0;	   /* ����ͨ�������ı�־, ����Ժ��յ���У��ϸ�����������Ϊ1 */
}

/*
*********************************************************************************************************
*	�� �� ��: gps_pro
*	����˵��: ��ѯGPS���ݰ������뵽��������ִ�м��ɡ�������������ȫ�ֱ��� g_tGPS
*	��    ��:  ��
*	�� �� ֵ: ��
*********************************************************************************************************
*/
void gps_pro(uint8_t *ucaGpsBuf,uint16_t num)
{
	uint8_t ucGpsHead = 0;
	uint8_t ucaGpsBuff[512];
	uint16_t usGpsPos = 0;
	uint16_t i = 0;
	/* �� GPSģ�鴮�ڶ�ȡ1���ֽ� comGetChar() ������ bsp_uart_fifo.c ʵ�� */
	
	for(i = 0; i < num; i ++)      //һ���Է���һ������
	{
		if (ucGpsHead == 0)
		{
			if (ucaGpsBuf[i] == '$')
			{
				ucGpsHead = 1;
				usGpsPos = 0;
			}
		}else{
			ucaGpsBuff[usGpsPos++] = ucaGpsBuf[i];
			if ((ucaGpsBuf[i] == '\r') || (ucaGpsBuf[i] == '\n'))
			{
				//printf("i am here\n");
				Analyze0183(ucaGpsBuff, usGpsPos-1);
				ucGpsHead = 0;
				usGpsPos = 0;
				g_tGPS.UartOk = 1;	              /* ���յ���ȷ������ */
			}
		}
	}
}

/*
*********************************************************************************************************
*	�� �� ��: CheckXor
*	����˵��: ���0183���ݰ���У����Ƿ���ȷ
*	��    ��:  _ucaBuf  �յ�������
*			 _usLen    ���ݳ���
*	�� �� ֵ: TRUE �� FALSE.
*********************************************************************************************************
*/
uint8_t CheckXor(uint8_t *_ucaBuf, uint16_t _usLen)
{
	uint8_t ucXorSum;
	uint8_t ucaBuf[2];
	uint16_t i;

	if (_usLen < 3)
	{
		return FALSE;
	}

	/* ���û��У���ֽڣ�Ҳ��Ϊ���� */
	if (_ucaBuf[_usLen - 3] != '*')
	{
		return FALSE;
	}


	/* ���������ַ�ASCII�ַ� */
	for (i = 0; i < _usLen - 3; i++)
	{
		if ((_ucaBuf[i] & 0x80) || (_ucaBuf[i] == 0))
		{
			return FALSE;
		}
	}

	ucXorSum = _ucaBuf[0];
	for (i = 1; i < _usLen - 3; i++)
	{
		ucXorSum = ucXorSum ^ _ucaBuf[i];
	}

	HexToAscii(&ucXorSum, ucaBuf, 2);

	if (memcmp(&_ucaBuf[_usLen - 2], ucaBuf, 2) == 0)
	{
		return TRUE;
	}

	return FALSE;
}

/*
*********************************************************************************************************
*	�� �� ��: gpsGPGGA
*	����˵��: ����0183���ݰ��е� GPGGA ��������ŵ�ȫ�ֱ���
*	��    ��:  _ucaBuf  �յ�������
*			 _usLen    ���ݳ���
*	�� �� ֵ: ��
*********************************************************************************************************
*/
/*
����$GPGGA,092204.999,4250.5589,S,14718.5084,E,1,04,24.4,19.7,M,,,,0000*1F
�ֶ�0��$GPGGA�����ID�����������ΪGlobal Positioning System Fix Data��GGA��GPS��λ��Ϣ
�ֶ�1��UTC ʱ�䣬hhmmss.sss��ʱ�����ʽ
�ֶ�2��γ��ddmm.mmmm���ȷָ�ʽ��ǰ��λ��������0��
�ֶ�3��γ��N����γ����S����γ��
�ֶ�4������dddmm.mmmm���ȷָ�ʽ��ǰ��λ��������0��
�ֶ�5������E����������W��������
�ֶ�6��GPS״̬��0=δ��λ��1=�ǲ�ֶ�λ��2=��ֶ�λ��3=��ЧPPS��6=���ڹ���
�ֶ�7������ʹ�õ�����������00 - 12����ǰ��λ��������0��
�ֶ�8��HDOPˮƽ�������ӣ�0.5 - 99.9��
�ֶ�9�����θ߶ȣ�-9999.9 - 99999.9��
�ֶ�10��������������Դ��ˮ׼��ĸ߶�
�ֶ�11�����ʱ�䣨�����һ�ν��յ�����źſ�ʼ��������������ǲ�ֶ�λ��Ϊ�գ�
�ֶ�12�����վID��0000 - 1023��ǰ��λ��������0��������ǲ�ֶ�λ��Ϊ�գ�
�ֶ�13��У��ֵ
*/
void gpsGPGGA(uint8_t *_ucaBuf, uint16_t _usLen)
{
	char *p;

	p = (char *)_ucaBuf;
	p[_usLen] = 0;

	/* �ֶ�1 UTC ʱ�䣬hhmmss.sss��ʱ�����ʽ */
	p = strchr(p, ',');
	if (p == 0)
	{
		return;
	}
	p++;
	if( *p != ',')
	{
		g_tGPS.Hour = StrToIntFix(p, 2);
		p += 2;
		g_tGPS.Min = StrToIntFix(p, 2);
		p += 2;
		g_tGPS.Sec = StrToIntFix(p, 2);
		p += 2;
	}
	/* �ֶ�2 γ��ddmm.mmmm���ȷָ�ʽ��ǰ��λ��������0�� */
	p = strchr(p, ',');
	if (p == 0)
	{
		return;
	}
	p++;
	if( *p != ',')
	{
		g_tGPS.WeiDu_Du = StrToIntFix(p, 2);
		p += 2;
		g_tGPS.WeiDu_Fen = StrToIntFix(p, 2) * 10000;
		p += 3;
		g_tGPS.WeiDu_Fen += StrToIntFix(p, 4);
		p += 4;
	}
	/* �ֶ�3 γ��N����γ����S����γ�� */
	p = strchr(p, ',');
	if (p == 0)
	{
		return;
	}
	p++;

	if (*p == 'S')
	{
		g_tGPS.NS = 'S';
	}
	else if (*p == 'N')
	{
		g_tGPS.NS = 'N';
	}
	else
	{
		return;
	}


	/* �ֶ�4  ����dddmm.mmmm���ȷָ�ʽ��ǰ��λ��������0�� */
	p = strchr(p, ',');
	if (p == 0)
	{
		return;
	}
	p++;
	if( *p != ',')
	{
		g_tGPS.JingDu_Du = StrToIntFix(p, 3);
		p += 3;
		g_tGPS.JingDu_Fen = StrToIntFix(p, 2) * 10000;
		p += 3;
		g_tGPS.JingDu_Fen += StrToIntFix(p, 4);
		p += 4;
	}
	/* �ֶ�5 ����E����������W�������� */
	p = strchr(p, ',');
	if (p == 0)
	{
		return;
	}
	p++;

	if (*p == 'E')
	{
		g_tGPS.EW = 'E';
	}
	else if (*p == 'W')
	{
		g_tGPS.EW = 'W';
	}else{
		return;
	}


	/* �ֶ�6 GPS״̬��0=δ��λ��1=�ǲ�ֶ�λ��2=��ֶ�λ��3=��ЧPPS��6=���ڹ��� */
	p = strchr(p, ',');
	if (p == 0)
	{
		return;
	}
	p++;
	if ((*p == '1') || (*p == '2'))
	{
		g_tGPS.PositionOk = 1;
	}
	else
	{
		g_tGPS.PositionOk = 0;
	}

	/* �ֶ�7������ʹ�õ�����������00 - 12����ǰ��λ��������0�� */
	p = strchr(p, ',');
	if (p == 0)
	{
		return;
	}
	p++;
	if(*p != ',')
	{
		g_tGPS.ViewNumber = StrToInt(p);
		p += 2;
	}
	/* �ֶ�8��HDOPˮƽ�������ӣ�0.5 - 99.9�� */
	p = strchr(p, ',');
	if (p == 0)
	{
		return;
	}
	p++;
	g_tGPS.HDOP = StrToInt(p);

	/* �ֶ�9�����θ߶ȣ�-9999.9 - 99999.9�� */
	p = strchr(p, ',');
	if (p == 0)
	{
		return;
	}
	p++;
	g_tGPS.Altitude = StrToInt(p);

	/* ������ֶ���Ϣ���� */
}

/*
*********************************************************************************************************
*	�� �� ��: gpsGPGSA
*	����˵��: ����0183���ݰ��е� GPGSA ��������ŵ�ȫ�ֱ���
*	��    ��:  _ucaBuf  �յ�������
*			 _usLen    ���ݳ���
*	�� �� ֵ: ��
*********************************************************************************************************
*/
/*
����$GPGSA,A,3,01,20,19,13,,,,,,,,,40.4,24.4,32.2*0A
�ֶ�0��$GPGSA�����ID�����������ΪGPS DOP and Active Satellites��GSA����ǰ������Ϣ
�ֶ�1����λģʽ��A=�Զ��ֶ�2D/3D��M=�ֶ�2D/3D
�ֶ�2����λ���ͣ�1=δ��λ��2=2D��λ��3=3D��λ
�ֶ�3��PRN�루α��������룩����1�ŵ�����ʹ�õ�����PRN���ţ�00����ǰ��λ��������0��
�ֶ�4��PRN�루α��������룩����2�ŵ�����ʹ�õ�����PRN���ţ�00����ǰ��λ��������0��
�ֶ�5��PRN�루α��������룩����3�ŵ�����ʹ�õ�����PRN���ţ�00����ǰ��λ��������0��
�ֶ�6��PRN�루α��������룩����4�ŵ�����ʹ�õ�����PRN���ţ�00����ǰ��λ��������0��
�ֶ�7��PRN�루α��������룩����5�ŵ�����ʹ�õ�����PRN���ţ�00����ǰ��λ��������0��
�ֶ�8��PRN�루α��������룩����6�ŵ�����ʹ�õ�����PRN���ţ�00����ǰ��λ��������0��
�ֶ�9��PRN�루α��������룩����7�ŵ�����ʹ�õ�����PRN���ţ�00����ǰ��λ��������0��
�ֶ�10��PRN�루α��������룩����8�ŵ�����ʹ�õ�����PRN���ţ�00����ǰ��λ��������0��
�ֶ�11��PRN�루α��������룩����9�ŵ�����ʹ�õ�����PRN���ţ�00����ǰ��λ��������0��
�ֶ�12��PRN�루α��������룩����10�ŵ�����ʹ�õ�����PRN���ţ�00����ǰ��λ��������0��
�ֶ�13��PRN�루α��������룩����11�ŵ�����ʹ�õ�����PRN���ţ�00����ǰ��λ��������0��
�ֶ�14��PRN�루α��������룩����12�ŵ�����ʹ�õ�����PRN���ţ�00����ǰ��λ��������0��
�ֶ�15��PDOP�ۺ�λ�þ������ӣ�0.5 - 99.9��
�ֶ�16��HDOPˮƽ�������ӣ�0.5 - 99.9��
�ֶ�17��VDOP��ֱ�������ӣ�0.5 - 99.9��
�ֶ�18��У��ֵ
*/
void gpsGPGSA(uint8_t *_ucaBuf, uint16_t _usLen)
{
	char *p;
	uint8_t i;

	p = (char *)_ucaBuf;
	p[_usLen] = 0;

	/* �ֶ�1 ��λģʽ��A=�Զ��ֶ�2D/3D��M=�ֶ�2D/3D */
	p = strchr(p, ',');
	if (p == 0)
	{
		return;
	}
	p++;
	g_tGPS.ModeAM = *p;

	/* �ֶ�2 ��λ���ͣ�1=δ��λ��2=2D��λ��3=3D��λ */
	p = strchr(p, ',');
	if (p == 0)
	{
		return;
	}
	p++;
	g_tGPS.Mode2D3D = *p;

	/* �ֶ�3 - �ֶ�14 ��1-12�ŵ�����ʹ�õ�����PRN���� */
	for (i = 0; i < 12; i++)
	{
		p = strchr(p, ',');
		if (p == 0)
		{
			return;
		}
		p++;
		g_tGPS.SateID[i] = StrToInt(p);
	}

	/* �ֶ�15��PDOP�ۺ�λ�þ������ӣ�0.5 - 99.9�� */
	p = strchr(p, ',');
	if (p == 0)
	{
		return;
	}
	p++;
	g_tGPS.PDOP = StrToInt(p);

	/* �ֶ�16��HDOPˮƽ�������ӣ�0.5 - 99.9�� */
	p = strchr(p, ',');
	if (p == 0)
	{
		return;
	}
	p++;
	g_tGPS.HDOP = StrToInt(p);

	/* �ֶ�17��VDOP��ֱ�������ӣ�0.5 - 99.9�� */
	p = strchr(p, ',');
	if (p == 0)
	{
		return;
	}
	p++;
	g_tGPS.VDOP = StrToInt(p);
}

/*
*********************************************************************************************************
*	�� �� ��: gpsGPGSV
*	����˵��: ����0183���ݰ��е� GPGSV ��������ŵ�ȫ�ֱ���
*	��    ��:  _ucaBuf  �յ�������
*			 _usLen    ���ݳ���
*	�� �� ֵ: ��
*********************************************************************************************************
*/
/*
����$GPGSV,3,1,10,20,78,331,45,01,59,235,47,22,41,069,,13,32,252,45*70

$GPGSV,2,1,07,07,79,048,42,02,51,062,43,26,36,256,42,27,27,138,42*71
$GPGSV,2,2,07,09,23,313,42,04,19,159,41,15,12,041,42*41

�ֶ�0��$GPGSV�����ID�����������ΪGPS Satellites in View��GSV���ɼ�������Ϣ
�ֶ�1������GSV��������Ŀ��1 - 3��
�ֶ�2������GSV����Ǳ���GSV���ĵڼ�����1 - 3��
�ֶ�3����ǰ�ɼ�����������00 - 12����ǰ��λ��������0��

�ֶ�4��PRN �루α��������룩��01 - 32����ǰ��λ��������0��
�ֶ�5���������ǣ�00 - 90���ȣ�ǰ��λ��������0��
�ֶ�6�����Ƿ�λ�ǣ�00 - 359���ȣ�ǰ��λ��������0��
�ֶ�7������ȣ�00��99��dbHz

�ֶ�8��PRN �루α��������룩��01 - 32����ǰ��λ��������0��
�ֶ�9���������ǣ�00 - 90���ȣ�ǰ��λ��������0��
�ֶ�10�����Ƿ�λ�ǣ�00 - 359���ȣ�ǰ��λ��������0��
�ֶ�11������ȣ�00��99��dbHz

�ֶ�12��PRN �루α��������룩��01 - 32����ǰ��λ��������0��
�ֶ�13���������ǣ�00 - 90���ȣ�ǰ��λ��������0��
�ֶ�14�����Ƿ�λ�ǣ�00 - 359���ȣ�ǰ��λ��������0��
�ֶ�15������ȣ�00��99��dbHz
�ֶ�16��У��ֵ
*/
void gpsGPGSV(uint8_t *_ucaBuf, uint16_t _usLen)
{
//	uint8_t s_total = 0;	/* �������Ŀ */
	uint8_t s_no = 0;		/* ������ */
	uint8_t i;
	char *p;

	p = (char *)_ucaBuf;
	p[_usLen] = 0;

	/* �ֶ�1������GSV��������Ŀ��1 - 3�� */
	p = strchr(p, ',');
	if (p == 0)
	{
		return;
	}
	p++;
//	s_total = StrToInt(p);

	/* �ֶ�2������GSV����Ǳ���GSV���ĵڼ�����1 - 3�� */
	p = strchr(p, ',');
	if (p == 0)
	{
		return;
	}
	p++;
	s_no = StrToInt(p);

	/* �ֶ�3����ǰ�ɼ�����������00 - 12����ǰ��λ��������0�� */
	p = strchr(p, ',');
	if (p == 0)
	{
		return;
	}
	p++;
	g_tGPS.ViewNumber = StrToInt(p);

	for (i = 0; i < 4; i++)
	{
		/* �ֶ�4��PRN �루α��������룩��01 - 32����ǰ��λ��������0�� */
		p = strchr(p, ',');
		if (p == 0)
		{
			return;
		}
		p++;
		g_tGPS.SateID[(s_no - 1) * 4 + i] = StrToInt(p);

		/* �ֶ�5���������ǣ�00 - 90���ȣ�ǰ��λ��������0��*/
		p = strchr(p, ',');
		if (p == 0)
		{
			return;
		}
		p++;
		g_tGPS.Elevation[(s_no - 1) * 4 + i] = StrToInt(p);

		/* �ֶ�6�����Ƿ�λ�ǣ�00 - 359���ȣ�ǰ��λ��������0�� */
		p = strchr(p, ',');
		if (p == 0)
		{
			return;
		}
		p++;
		g_tGPS.Azimuth[(s_no - 1) * 4 + i] = StrToInt(p);

		/* �ֶ�7������ȣ�00��99��dbHz */
		p = strchr(p, ',');
		if (p == 0)
		{
			return;
		}
		p++;
		g_tGPS.SNR[(s_no - 1) * 4 + i] = StrToInt(p);
	}
}

/*
*********************************************************************************************************
*	�� �� ��: gpsGPRMC
*	����˵��: ����0183���ݰ��е� GPGSV ��������ŵ�ȫ�ֱ���
*	��    ��:  _ucaBuf  �յ�������
*			 _usLen    ���ݳ���
*	�� �� ֵ: ��
*********************************************************************************************************
*/
/*
����$GPRMC,024813.640,A,3158.4608,N,11848.3737,E,10.05,324.27,150706,,,A*50
�ֶ�0��$GPRMC�����ID�����������ΪRecommended Minimum Specific GPS/TRANSIT Data��RMC���Ƽ���С��λ��Ϣ
�ֶ�1��UTCʱ�䣬hhmmss.sss��ʽ
�ֶ�2��״̬��A=��λ��V=δ��λ
�ֶ�3��γ��ddmm.mmmm���ȷָ�ʽ��ǰ��λ��������0��
�ֶ�4��γ��N����γ����S����γ��
�ֶ�5������dddmm.mmmm���ȷָ�ʽ��ǰ��λ��������0��
�ֶ�6������E����������W��������
�ֶ�7���ٶȣ��ڣ�Knots
�ֶ�8����λ�ǣ���
�ֶ�9��UTC���ڣ�DDMMYY��ʽ
�ֶ�10����ƫ�ǣ���000 - 180���ȣ�ǰ��λ��������0��
�ֶ�11����ƫ�Ƿ���E=��W=��
�ֶ�16��У��ֵ
*/
void gpsGPRMC(uint8_t *_ucaBuf, uint16_t _usLen)
{
	char *p;

	p = (char *)_ucaBuf;
	p[_usLen] = 0;

	/* �ֶ�1 UTCʱ�䣬hhmmss.sss��ʽ */
	p = strchr(p, ',');
	if (p == 0)
	{
		return;
	}
	p++;
	if(*p != ',')
	{
		g_tGPS.Hour = StrToIntFix(p, 2);
		p += 2;
		g_tGPS.Min = StrToIntFix(p, 2);
		p += 2;
		g_tGPS.Sec = StrToIntFix(p, 2);
		p += 3;
		g_tGPS.mSec = StrToIntFix(p, 3);
	}
	/* �ֶ�2 ״̬��A=��λ��V=δ��λ */
	p = strchr(p, ',');
	if (p == 0)
	{
		return;
	}
	p++;
	if (*p != 'A')
	{
		/* δ��λ��ֱ�ӷ��� */
		g_tGPS.PositionOk = 0;
		return;
	}
	g_tGPS.PositionOk = 1;
//	p += 1;

	/* �ֶ�3 γ��ddmm.mmmm���ȷָ�ʽ��ǰ��λ��������0�� */
	p = strchr(p, ',');
	if (p == 0)
	{
		return;
	}
	p++;
	if(*p != ',')
	{
		g_tGPS.WeiDu_Du = StrToIntFix(p, 2);
		p += 2;
		g_tGPS.WeiDu_Fen = StrToIntFix(p, 2) * 10000;
		p += 3;
		g_tGPS.WeiDu_Fen += StrToIntFix(p, 4);
		p += 4;
	}

	/* �ֶ�4 γ��N����γ����S����γ��*/
	p = strchr(p, ',');
	if (p == 0)
	{
		return;
	}
	p++;
	if (*p == 'S')
	{
		g_tGPS.NS = 'S';
	}
	else if (*p == 'N')
	{
		g_tGPS.NS = 'N';
	}
	else
	{
		return;
	}

	/* �ֶ�5 ����dddmm.mmmm���ȷָ�ʽ��ǰ��λ��������0�� */
	p = strchr(p, ',');
	if (p == 0)
	{
		return;
	}
	p++;
	if(*p != ',')
	{
		g_tGPS.JingDu_Du = StrToIntFix(p, 3);
		p += 3;
		g_tGPS.JingDu_Fen = StrToIntFix(p, 2) * 10000;
		p += 3;
		g_tGPS.JingDu_Fen += StrToIntFix(p, 4);
		p += 4;
	}

	/* �ֶ�6������E����������W�������� */
	p = strchr(p, ',');
	if (p == 0)
	{
		return;
	}
	p++;
	if (*p == 'E')
	{
		g_tGPS.EW = 'E';
	}
	else if (*p == 'W')
	{
		g_tGPS.EW = 'W';
	}

	/* �ֶ�7���ٶȣ��ڣ�Knots  10.05,*/
	p = strchr(p, ',');
	if (p == 0)
	{
		return;
	}
	p++;
	g_tGPS.SpeedKnots = StrToInt(p);

	/* �ֶ�8����λ�ǣ��� ,324.27 */
	p = strchr(p, ',');
	if (p == 0)
	{
		return;
	}
	p++;
	g_tGPS.TrackDegTrue = StrToInt(p);

	/* �ֶ�9��UTC���ڣ�DDMMYY��ʽ  150706 */
	p = strchr(p, ',');
	if (p == 0)
	{
		return;
	}
	p++;
	if(*p != ',')
	{
		g_tGPS.Day = StrToIntFix(p, 2);
		p += 2;
		g_tGPS.Month = StrToIntFix(p, 2);
		p += 2;
		g_tGPS.Year = StrToIntFix(p, 2);
		p += 2;
	}
}

/*
*********************************************************************************************************
*	�� �� ��: gpsGPVTG
*	����˵��: ����0183���ݰ��е� GPVTG ��������ŵ�ȫ�ֱ���
*	��    ��:  _ucaBuf  �յ�������
*			 _usLen    ���ݳ���
*	�� �� ֵ: ��
*********************************************************************************************************
*/
/*
����$GPVTG,89.68,T,,M,0.00,N,0.0,K*5F
�ֶ�0��$GPVTG�����ID�����������ΪTrack Made Good and Ground Speed��VTG�������ٶ���Ϣ
�ֶ�1���˶��Ƕȣ�000 - 359����ǰ��λ��������0��
�ֶ�2��T=�汱����ϵ
�ֶ�3���˶��Ƕȣ�000 - 359����ǰ��λ��������0��
�ֶ�4��M=�ű�����ϵ
�ֶ�5��ˮƽ�˶��ٶȣ�0.00����ǰ��λ��������0��
�ֶ�6��N=�ڣ�Knots
�ֶ�7��ˮƽ�˶��ٶȣ�0.00����ǰ��λ��������0��
�ֶ�8��K=����/ʱ��km/h
�ֶ�9��У��ֵ
*/
void gpsGPVTG(uint8_t *_ucaBuf, uint16_t _usLen)
{
	char *p;

	p = (char *)_ucaBuf;
	p[_usLen] = 0;

	/* �ֶ�1���˶��Ƕȣ�000 - 359����ǰ��λ��������0��*/
	p = strchr(p, ',');
	if (p == 0)
	{
		return;
	}
	p++;
	g_tGPS.TrackDegTrue = StrToInt(p);

	/* �ֶ�2��T=�汱����ϵ */
	p = strchr(p, ',');
	if (p == 0)
	{
		return;
	}
	p++;

	/* �ֶ�3���˶��Ƕȣ�000 - 359����ǰ��λ��������0�� */
	p = strchr(p, ',');
	if (p == 0)
	{
		return;
	}
	p++;
	g_tGPS.TrackDegMag = StrToInt(p);

	/* �ֶ�4��M=�ű�����ϵ */
	p = strchr(p, ',');
	if (p == 0)
	{
		return;
	}
	p++;

	/* �ֶ�5���������ʣ�000.0~999.9�ڣ�ǰ���0Ҳ�������䣩 */
	p = strchr(p, ',');
	if (p == 0)
	{
		return;
	}
	p++;
	g_tGPS.SpeedKnots = StrToInt(p);

	/* �ֶ�6��N=�ڣ�Knots */
	p = strchr(p, ',');
	if (p == 0)
	{
		return;
	}
	p++;

	/* �ֶ�7���������ʣ�0000.0~1851.8����/Сʱ��ǰ���0Ҳ�������䣩 */
	p = strchr(p, ',');
	if (p == 0)
	{
		return;
	}
	p++;
	g_tGPS.SpeedKM = StrToInt(p);

	/* �ֶ�8��K=����/ʱ��km/h	 */
}

/*
*********************************************************************************************************
*	�� �� ��: gpsGPGLL
*	����˵��: ����0183���ݰ��е� GPGLL ��������ŵ�ȫ�ֱ���
*	��    ��:  _ucaBuf  �յ�������
*			 _usLen    ���ݳ���
*	�� �� ֵ: ��
*********************************************************************************************************
*/
/*
����$GPGLL,4250.5589,S,14718.5084,E,092204.999,A*2D
�ֶ�0��$GPGLL�����ID�����������ΪGeographic Position��GLL��������λ��Ϣ
�ֶ�1��γ��ddmm.mmmm���ȷָ�ʽ��ǰ��λ��������0��
�ֶ�2��γ��N����γ����S����γ��
�ֶ�3������dddmm.mmmm���ȷָ�ʽ��ǰ��λ��������0��
�ֶ�4������E����������W��������
�ֶ�5��UTCʱ�䣬hhmmss.sss��ʽ
�ֶ�6��״̬��A=��λ��V=δ��λ
�ֶ�7��У��ֵ
*/
void gpsGPGLL(uint8_t *_ucaBuf, uint16_t _usLen)
{
	char *p;

	p = (char *)_ucaBuf;
	p[_usLen] = 0;

	/* �ֶ�1 γ��ddmm.mmmm���ȷָ�ʽ��ǰ��λ��������0�� */
	p = strchr(p, ',');
	if (p == 0)
	{
		return;
	}
	p++;
	if(*p != ',')
	{
		g_tGPS.WeiDu_Du = StrToIntFix(p, 2);
		p += 2;
		g_tGPS.WeiDu_Fen = StrToIntFix(p, 2) * 10000;
		p += 3;
		g_tGPS.WeiDu_Fen += StrToIntFix(p, 4);
		p += 4;
	}

	/* �ֶ�2 γ��N����γ����S����γ��*/
	p = strchr(p, ',');
	if (p == 0)
	{
		return;
	}
	p++;
	if (*p == 'S')
	{
		g_tGPS.NS = 'S';
	}
	else if (*p == 'N')
	{
		g_tGPS.NS = 'N';
	}
	else
	{
		return;
	}

	/* �ֶ�3 ����dddmm.mmmm���ȷָ�ʽ��ǰ��λ��������0�� */
	p = strchr(p, ',');
	if (p == 0)
	{
		return;
	}
	p++;
	if(*p != ',')
	{
		g_tGPS.JingDu_Du = StrToIntFix(p, 3);
		p += 3;
		g_tGPS.JingDu_Fen = StrToIntFix(p, 2) * 10000;
		p += 3;
		g_tGPS.JingDu_Fen += StrToIntFix(p, 4);
		p += 4;
	}

	/* �ֶ�4������E����������W�������� */
	p = strchr(p, ',');
	if (p == 0)
	{
		return;
	}
	p++;
	if (*p == 'E')
	{
		g_tGPS.EW = 'E';
	}
	else if (*p == 'W')
	{
		g_tGPS.EW = 'W';
	}

	/* �ֶ�5 UTCʱ�䣬hhmmss.sss��ʽ */
	p = strchr(p, ',');
	if (p == 0)
	{
		return;
	}
	p++;
	if(*p != ',')
	{
		g_tGPS.Hour = StrToIntFix(p, 2);
		p += 2;
		g_tGPS.Min = StrToIntFix(p, 2);
		p += 2;
		g_tGPS.Sec = StrToIntFix(p, 2);
		p += 2;
	}
	/* �ֶ�6 ״̬��A=��λ��V=δ��λ */
	p = strchr(p, ',');
	if (p == 0)
	{
		return;
	}
	p++;
	if (*p != 'A')
	{
		/* δ��λ��ֱ�ӷ��� */
		return;
	}
}
void gpsKSXT(uint8_t *_ucaBuf, uint16_t _usLen)
{
	char *p;

	p = (char *)_ucaBuf;
	p[_usLen] = 0;

	/* �ֶ�1 UTCʱ�䣬hhmmss.sss��ʽ */
	p = strchr(p, ',');
	if (p == 0)
	{
		return;
	}
	p ++;
	/* 这中间是时间，跳过 */
	p = strchr(p, ',');
	p ++;
	/* 经度 */
	g_tGPS.longitude = atof(p);
	p = strchr(p, ',');
	p++;
	/* 纬度 */
	g_tGPS.latitude = atof(p);
	p = strchr(p, ',');
	p++;
	/* 高度 */
	g_tGPS.height = atof(p);
	p = strchr(p, ',');
	p++;
	/* 方向角 */
	g_tGPS.Yaw = atof(p);
	/* 到此结束，后面不在解析 */

}
/*
����$GNRMC,075707.80,A,3212.22332809,N,11930.74960356,E,0.020,20.623,240518,5.8657,W,D*35
�ֶ�0��$GPRMC�����ID�����������ΪRecommended Minimum Specific GPS/TRANSIT Data��RMC���Ƽ���С��λ��Ϣ
�ֶ�1��UTCʱ�䣬hhmmss.sss��ʽ
�ֶ�2��״̬��A=��λ��V=δ��λ
�ֶ�3��γ��ddmm.mmmmmmmm���ȷָ�ʽ��ǰ��λ��������0��
�ֶ�4��γ��N����γ����S����γ��
�ֶ�5������dddmm.mmmmmmmm���ȷָ�ʽ��ǰ��λ��������0��
�ֶ�6������E����������W��������
�ֶ�7���ٶȣ��ڣ�Knots
�ֶ�8����λ�ǣ���
�ֶ�9��UTC���ڣ�DDMMYY��ʽ
�ֶ�10����ƫ�ǣ���000 - 180���ȣ�ǰ��λ��������0��
�ֶ�11����ƫ�Ƿ���E=��W=��
�ֶ�16��У��ֵ
*/
void gpsGNRMC(uint8_t *_ucaBuf, uint16_t _usLen)
{
	char *p;

	p = (char *)_ucaBuf;
	p[_usLen] = 0;

	/* �ֶ�1 UTCʱ�䣬hhmmss.sss��ʽ */
	p = strchr(p, ',');
	if (p == 0)
	{
		return;
	}
	p++;
	if(*p != ',')
	{
		g_tGPS.Hour = StrToIntFix(p, 2);
		p += 2;
		g_tGPS.Min = StrToIntFix(p, 2);
		p += 2;
		g_tGPS.Sec = StrToIntFix(p, 2);
		p += 3;
		g_tGPS.mSec = StrToIntFix(p, 3);
	}
	/* �ֶ�2 ״̬��A=��λ��V=δ��λ */
	p = strchr(p, ',');
	if (p == 0)
	{
		return;
	}
	p++;
	if (*p != 'A')
	{
		/* δ��λ��ֱ�ӷ��� */
		g_tGPS.PositionOk = 0;
		return;
	}
	g_tGPS.PositionOk = 1;
//	p += 1;

	/* �ֶ�3 γ��ddmm.mmmm���ȷָ�ʽ��ǰ��λ��������0�� */
	p = strchr(p, ',');
	if (p == 0)
	{
		return;
	}
	p++;
	if(*p != ',')
	{
		g_tGPS.WeiDu_Du = StrToIntFix(p, 2);
		p += 2;
		g_tGPS.WeiDu_Fen = StrToIntFix(p, 2) * 100000000;
		p += 3;		//��С���� ȥ��
		g_tGPS.WeiDu_Fen += StrToIntFix(p, 8);
		p += 8;
	}

	/* �ֶ�4 γ��N����γ����S����γ��*/
	p = strchr(p, ',');
	if (p == 0)
	{
		return;
	}
	p++;
	if (*p == 'S')
	{
		g_tGPS.NS = 'S';
	}
	else if (*p == 'N')
	{
		g_tGPS.NS = 'N';
	}
	else
	{
		return;
	}

	/* �ֶ�5 ����dddmm.mmmm���ȷָ�ʽ��ǰ��λ��������0�� */
	p = strchr(p, ',');
	if (p == 0)
	{
		return;
	}
	p++;
	if(*p != ',')
	{
		g_tGPS.JingDu_Du = StrToIntFix(p, 3);
		p += 3;
		g_tGPS.JingDu_Fen = StrToIntFix(p, 2) * 100000000;
		p += 3;
		g_tGPS.JingDu_Fen += StrToIntFix(p, 8);
		p += 8;
	}

	/* �ֶ�6������E����������W�������� */
	p = strchr(p, ',');
	if (p == 0)
	{
		return;
	}
	p++;
	if (*p == 'E')
	{
		g_tGPS.EW = 'E';
	}
	else if (*p == 'W')
	{
		g_tGPS.EW = 'W';
	}

	/* �ֶ�7���ٶȣ��ڣ�Knots  10.05,*/
	p = strchr(p, ',');
	if (p == 0)
	{
		return;
	}
	p++;
	g_tGPS.SpeedKnots = StrToInt(p);

	/* �ֶ�8����λ�ǣ��� ,324.27 */
	p = strchr(p, ',');
	if (p == 0)
	{
		return;
	}
	p++;
	g_tGPS.TrackDegTrue = StrToInt(p);

	/* �ֶ�9��UTC���ڣ�DDMMYY��ʽ  150706 */
	p = strchr(p, ',');
	if (p == 0)
	{
		return;
	}
	p++;
	if(*p != ',')
	{
		g_tGPS.Day = StrToIntFix(p, 2);
		p += 2;
		g_tGPS.Month = StrToIntFix(p, 2);
		p += 2;
		g_tGPS.Year = StrToIntFix(p, 2);
		p += 2;
	}
	/* ���滹���ֶ�û�з��� */
}

/*
����$GNHDT,211.023,T*28
�ֶ�0��$GNHDT  ��λ��
�ֶ�1����λ��  ��ʽ  δ֪
�ֶ�2��У��ֵ
*/
void gpsGNHDT(uint8_t *_ucaBuf, uint16_t _usLen)
{
	char *p;
	char *pEnd;
	p = (char *)_ucaBuf;
	p[_usLen] = 0;

	/* �ֶ�1����ʽ  δ֪ */
	p = strchr(p, ',');
	if (p == 0)
	{
		return;
	}
	p++;
	if(*p != ',')
	{
		g_tGPS.Yaw = strtof(p,&pEnd);
	}
	/* �ֶ�2��У��ֵ */
}


/*
*********************************************************************************************************
*	�� �� ��: Analyze0183
*	����˵��: ����0183���ݰ�
*	��    ��:  _ucaBuf  �յ�������
*			 _usLen    ���ݳ���
*	�� �� ֵ: ��
*********************************************************************************************************
*/
void Analyze0183(uint8_t *_ucaBuf, uint16_t _usLen)
{

	// if (CheckXor(_ucaBuf, _usLen) != TRUE)
	// {
	// 	return;
	// }
	if(memcmp(_ucaBuf, "KSXT,", 5) == 0){
		gpsKSXT(_ucaBuf, _usLen);
	}else
	if(memcmp(_ucaBuf, "GNHDT,", 6) == 0)   //������λ��
	{
		gpsGNHDT(_ucaBuf, _usLen);
	}
	else if(memcmp(_ucaBuf, "GNRMC,", 6) == 0)   //���� ��γ��  RTKģʽ��  ���׼�
	{
		gpsGNRMC(_ucaBuf, _usLen);
	}
	else if(memcmp(_ucaBuf, "GPHDT,", 6) == 0)   //������λ��
	{
		gpsGNHDT(_ucaBuf, _usLen);
	}
	else if(memcmp(_ucaBuf, "GPRMC,", 6) == 0)   //���� ��γ��  RTKģʽ��  ���׼�
	{
		gpsGNRMC(_ucaBuf, _usLen);
	}
	else{
		g_tGPS.PositionOk = 0;   //��λʧ��
	}
	
//	else if (memcmp(_ucaBuf, "GPGGA,", 6) == 0)
//	{
//		gpsGPGGA(_ucaBuf, _usLen);
//	}
//	else if (memcmp(_ucaBuf, "GPGSA,", 6) == 0)
//	{
//		gpsGPGSA(_ucaBuf, _usLen);
//	}
//	else if (memcmp(_ucaBuf, "GPGSV,", 6) == 0)
//	{
//		gpsGPGSV(_ucaBuf, _usLen);
//	}
//	else if (memcmp(_ucaBuf, "GPRMC,", 6) == 0)
//	{
//		//gpsGPRMC(_ucaBuf, _usLen);
//	}
//	else if (memcmp(_ucaBuf, "GPVTG,", 6) == 0)
//	{
//		gpsGPVTG(_ucaBuf, _usLen);
//	}
//	else if (memcmp(_ucaBuf, "GPGLL,", 6) == 0)
//	{
//		gpsGPGLL(_ucaBuf, _usLen);
//	}
}

/*
*********************************************************************************************************
*	�� �� ��: StrToInt
*	����˵��: ��ASCII���ַ���ת����ʮ����
*	��    ��: _pStr :��ת����ASCII�봮. �����Զ��Ż�0����
*	�� �� ֵ: ����������ֵ
*********************************************************************************************************
*/
int32_t StrToInt(char *_pStr)
{
	uint8_t flag;
	char *p;
	uint32_t ulInt;
	uint8_t i;
	uint8_t ucTemp;

	p = _pStr;
	if (*p == '-')
	{
		flag = 1;	/* ���� */
		p++;
	}
	else
	{
		flag = 0;
	}

	ulInt = 0;
	for (i = 0; i < 15; i++)
	{
		ucTemp = *p;
		if (ucTemp == '.')	/* ����С���㣬�Զ�����1���ֽ� */
		{
			p++;
			ucTemp = *p;
		}
		if ((ucTemp >= '0') && (ucTemp <= '9'))
		{
			ulInt = ulInt * 10 + (ucTemp - '0');
			p++;
		}
		else
		{
			break;
		}
	}

	if (flag == 1)
	{
		return -ulInt;
	}
	return ulInt;
}

/*
*********************************************************************************************************
*	�� �� ��: StrTofloat
*	����˵��: ��ASCII���ַ���ת����ʮ���Ƹ�����
*	��    ��: _pStr :��ת����ASCII�봮. �����Զ��Ż�0����
*	�� �� ֵ: ����������ֵ
*********************************************************************************************************
*/
float StrTofloat(char *_pStr)
{
	uint8_t flag;
	char *p;
	uint32_t ulInt;
	uint8_t i;
	uint8_t ucTemp;
	uint8_t floatFlag = 0;

	p = _pStr;
	if (*p == '-')
	{
		flag = 1;	/* ���� */
		p++;
	}
	else
	{
		flag = 0;
	}

	ulInt = 0;
	for (i = 0; i < 15; i++)
	{
		ucTemp = *p;
		if (ucTemp == '.')	/* ����С���㣬�Զ�����1���ֽ� */
		{
			p++;
			ucTemp = *p;
			if(floatFlag == 0){      //��������.  �϶���������������
				floatFlag = 1;
			}else{
				break;
			}
		}
		if ((ucTemp >= '0') && (ucTemp <= '9'))
		{
			ulInt = ulInt * 10 + (ucTemp - '0');
			p++;
			if(floatFlag){
				floatFlag ++;
			}
		}
		else
		{
			break;
		}
	}
	float floV = ulInt/((floatFlag - 1)*10.f);     //
	if (flag == 1)
	{
		return -floV;
	}
	return floV;
}

/*
*********************************************************************************************************
*	�� �� ��: StrToIntFix
*	����˵��: ��ASCII���ַ���ת����ʮ����, ��������
*	��    ��: _pStr :��ת����ASCII�봮. �����Զ��Ż�0����
*			 _ucLen : �̶�����
*	�� �� ֵ: ����������ֵ
*********************************************************************************************************
*/
int32_t StrToIntFix(char *_pStr, uint8_t _ucLen)
{
	uint8_t flag;
	char *p;
	uint32_t ulInt;
	uint8_t i;
	uint8_t ucTemp;

	p = _pStr;
	if (*p == '-')
	{
		flag = 1;	/* ���� */
		p++;
		_ucLen--;
	}
	else
	{
		flag = 0;
	}

	ulInt = 0;
	for (i = 0; i < _ucLen; i++)
	{
		ucTemp = *p;
		if (ucTemp == '.')	/* ����С���㣬�Զ�����1���ֽ� */
		{
			p++;
			ucTemp = *p;
		}
		if ((ucTemp >= '0') && (ucTemp <= '9'))
		{
			ulInt = ulInt * 10 + (ucTemp - '0');
			p++;
		}
		else
		{
			break;
		}
	}

	if (flag == 1)
	{
		return -ulInt;
	}
	return ulInt;
}

/*
*********************************************************************************************************
*	�� �� ��: HexToAscii
*	����˵��: ��hex��0x1fת����'1'��'f'. ��β��0.
*	��    ��: ucpHex ���뻺����ָ��
*		     _ucpAscII ���������ָ��
*		    _ucLenasc ASCII���ַ�����.
*	�� �� ֵ: ����������ֵ
*********************************************************************************************************
*/
void HexToAscii(uint8_t *_ucpHex, uint8_t *_ucpAscII, uint8_t _ucLenasc)
{
	uint8_t i;
	uint8_t ucTemp;

	for (i = 0; i < _ucLenasc; i++)
	{
		ucTemp = *_ucpHex;
		if ((i&0x01) == 0x00)
			ucTemp = ucTemp >> 4;
		else
		{
			ucTemp = ucTemp & 0x0f;
			_ucpHex++;
		}
		if (ucTemp < 0x0a)
			ucTemp += 0x30;
		else
			ucTemp += 0x37;
		_ucpAscII[i] = ucTemp;
	}
	//--------debug--------//
	_ucpAscII[i] = '\0';
	//--------end----------//
}

/*
*********************************************************************************************************
*	�� �� ��: gps_FenToDu
*	����˵��: ����ת��Ϊ�ȵ�С�����֣�����6λС���� ���ֻ���Ϊ�ȡ�
*	��    ��: ��
*	�� �� ֵ: ���ضȵ�С�����֣�ʮ���ƣ�
*********************************************************************************************************
*/
uint32_t gps_FenToDu(uint32_t _fen)
{
	uint32_t du;
	
	/* g_tGPS.WeiDu_Fen;	γ�ȣ���. 232475��  С�����4λ  ��ʾ 23.2475�� */
	
	du = (_fen * 100) / 60;
	
	return du;
}

/*
*********************************************************************************************************
*	�� �� ��: gps_FenToMiao
*	����˵��: ���ֵ�С������ת����
*	��    ��: ��
*	�� �� ֵ: �� ��������
*********************************************************************************************************
*/
uint16_t gps_FenToMiao(uint32_t _fen)
{
	uint32_t miao;
	
	/* g_tGPS.WeiDu_Fen;	γ�ȣ���. 232475��  С�����4λ  ��ʾ 23.2475�� 
		����С������ 0.2475 * 60 = 14.85 ��������Ϊ 15��	
		
		
		2475 * 60 = 148500
		148500 / 10000 = 14;
		
		if ((148500 % 10000) >= 5000)
		{
			miao = 14 + 1
		}
	*/
	
	miao = ((_fen % 10000) * 60);
	
	if ((miao % 10000) >= 5000)
	{
		miao = miao / 10000 + 1;	/* 5�� */
	}
	else
	{
		miao = miao / 10000;		/* 4�� */
	}
		
	return miao;
}



/*
*********************************************************************************************************
*	�� �� ��: StrToIntFix
*	����˵��: ��ASCII���ַ���ת����ʮ����, ��������
*	��    ��: _pStr :��ת����ASCII�봮. �����Զ��Ż�0����
*			 _ucLen : �̶�����
*	�� �� ֵ: ����������ֵ
*********************************************************************************************************
*/
void UTCDate(void)
{
	#if 0
	/* ����UTCʱ�� */
	{
		uint8_t ucaDays[]={31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

		if (g_tParam.iUTCtime > 0)
		{
			g_tGPS.Hour += g_tParam.iUTCtime;
			if (g_tGPS.Hour > 23)
			{
				g_tGPS.Hour = g_tGPS.Hour - 24;
				g_tGPS.ucDay++;

				/* ����2�·�Ϊ29�� */
				if (IsLeapYear(g_tGPS.usYear))
				{
					ucaDays[1] = 29;
				}
				else
				{
					ucaDays[1] = 28;
				}

				if (g_tGPS.ucDay > ucaDays[g_tGPS.ucMonth - 1])
				{
					g_tGPS.ucDay = 1;

					g_tGPS.ucMonth++;

					if (g_tGPS.ucMonth > 12)
					{
						g_tGPS.usYear++;
					}
				}
			}

		}
		else if (g_tParam.iUTCtime < 0)
		{
			int iHour;

			iHour = g_tGPS.Hour;
			iHour += g_tParam.iUTCtime;

			if (iHour < 0)
			{
				g_tGPS.Hour = 24 + iHour;

				if (g_tGPS.ucDay == 1)
				{
					if (g_tGPS.ucMonth == 1)
					{
						g_tGPS.usYear--;
						g_tGPS.ucMonth = 12;
						g_tGPS.ucDay = 31;
					}
					else
					{
						if (g_tGPS.ucMonth == 3)
						{
							g_tGPS.ucMonth = 2;

							/* ����2�·�Ϊ29�� */
							if (IsLeapYear(g_tGPS.usYear))
							{
								g_tGPS.ucDay = 29;
							}
							else
							{
								g_tGPS.ucDay = 28;
							}
						}
						else
						{
							g_tGPS.ucMonth--;

							g_tGPS.ucDay = ucaDays[g_tGPS.ucMonth];
						}
					}
				}
				else
				{
					g_tGPS.ucDay--;
				}
			}
			else
			{
				g_tGPS.Hour = iHour;
			}
		}
	}
	#endif
}


/*
GPS�ϵ��ÿ��һ����ʱ��ͻ᷵��һ����ʽ�����ݣ����ݸ�ʽΪ��

$��Ϣ���ͣ�x��x��x��x��x��x��x��x��x��x��x��x��x

ÿ�п�ͷ���ַ����ǡ�$������������Ϣ���ͣ����������ݣ��Զ��ŷָ�����һ���������������£�

    $GPRMC,080655.00,A,4546.40891,N,12639.65641,E,1.045,328.42,170809,,,A*60



��Ϣ����Ϊ��

GPGSV���ɼ�������Ϣ

GPGLL��������λ��Ϣ

GPRMC���Ƽ���С��λ��Ϣ

GPVTG�������ٶ���Ϣ

GPGGA��GPS��λ��Ϣ

GPGSA����ǰ������Ϣ



1�� GPS DOP and Active Satellites��GSA����ǰ������Ϣ

$GPGSA,<1>,<2>,<3>,<3>,,,,,<3>,<3>,<3>,<4>,<5>,<6>,<7>

<1>ģʽ ��M = �ֶ��� A = �Զ���
<2>��λ��ʽ 1 = δ��λ�� 2 = ��ά��λ�� 3 = ��ά��λ��
<3>PRN ���֣�01 �� 32 �����ʹ���е����Ǳ�ţ����ɽ���12��������Ϣ��
<4> PDOPλ�þ������ӣ�0.5~99.9��
<5> HDOPˮƽ�������ӣ�0.5~99.9��
<6> VDOP��ֱ�������ӣ�0.5~99.9��
<7> Checksum.(���λ).

2�� GPS Satellites in View��GSV���ɼ�������Ϣ

$GPGSV, <1>,<2>,<3>,<4>,<5>,<6>,<7>,?<4>,<5>,<6>,<7>,<8>

<1> GSV��������
<2> ����GSV�ı��
<3> �ɼ����ǵ�������00 �� 12��
<4> ���Ǳ�ţ� 01 �� 32��
<5>�������ǣ� 00 �� 90 �ȡ�
<6>���Ƿ�λ�ǣ� 000 �� 359 �ȡ�ʵ��ֵ��
<7>Ѷ�������ȣ�C/No���� 00 �� 99 dB���ޱ�δ���յ�Ѷ�š�
<8>Checksum.(���λ).
��<4>,<5>,<6>,<7>��������ǻ��ظ����֣�ÿ��������Ŀ����ǡ�����������Ϣ���ڴ�һ�г��֣���δʹ�ã���Щ�ֶλ�հס�


3��Global Positioning System Fix Data��GGA��GPS��λ��Ϣ
$GPGGA,<1>,<2>,<3>,<4>,<5>,<6>,<7>,<8>,<9>,M,<10>,M,<11>,<12>*hh

<1> UTCʱ�䣬hhmmss��ʱ���룩��ʽ
<2> γ��ddmm.mmmm���ȷ֣���ʽ��ǰ���0Ҳ�������䣩
<3> γ�Ȱ���N�������򣩻�S���ϰ���
<4> ����dddmm.mmmm���ȷ֣���ʽ��ǰ���0Ҳ�������䣩
<5> ���Ȱ���E����������W��������
<6> GPS״̬��0=δ��λ��1=�ǲ�ֶ�λ��2=��ֶ�λ��6=���ڹ���
<7> ����ʹ�ý���λ�õ�����������00~12����ǰ���0Ҳ�������䣩
<8> HDOPˮƽ�������ӣ�0.5~99.9��
<9> ���θ߶ȣ�-9999.9~99999.9��
<10> ������������Դ��ˮ׼��ĸ߶�
<11> ���ʱ�䣨�����һ�ν��յ�����źſ�ʼ��������������ǲ�ֶ�λ��Ϊ�գ�
<12> ���վID��0000~1023��ǰ���0Ҳ�������䣬������ǲ�ֶ�λ��Ϊ�գ�


4��Recommended Minimum Specific GPS/TRANSIT Data��RMC���Ƽ���λ��Ϣ
$GPRMC,<1>,<2>,<3>,<4>,<5>,<6>,<7>,<8>,<9>,<10>,<11>,<12>*hh

<1> UTCʱ�䣬hhmmss��ʱ���룩��ʽ
<2> ��λ״̬��A=��Ч��λ��V=��Ч��λ
<3> γ��ddmm.mmmm���ȷ֣���ʽ��ǰ���0Ҳ�������䣩
<4> γ�Ȱ���N�������򣩻�S���ϰ���
<5> ����dddmm.mmmm���ȷ֣���ʽ��ǰ���0Ҳ�������䣩
<6> ���Ȱ���E����������W��������
<7> �������ʣ�000.0~999.9�ڣ�ǰ���0Ҳ�������䣩
<8> ���溽��000.0~359.9�ȣ����汱Ϊ�ο���׼��ǰ���0Ҳ�������䣩
<9> UTC���ڣ�ddmmyy�������꣩��ʽ
<10> ��ƫ�ǣ�000.0~180.0�ȣ�ǰ���0Ҳ�������䣩
<11> ��ƫ�Ƿ���E��������W������
<12> ģʽָʾ����NMEA0183 3.00�汾�����A=������λ��D=��֣�E=���㣬N=������Ч��


5�� Track Made Good and Ground Speed��VTG�������ٶ���Ϣ
$GPVTG,<1>,T,<2>,M,<3>,N,<4>,K,<5>*hh

<1> ���汱Ϊ�ο���׼�ĵ��溽��000~359�ȣ�ǰ���0Ҳ�������䣩
<2> �Դű�Ϊ�ο���׼�ĵ��溽��000~359�ȣ�ǰ���0Ҳ�������䣩
<3> �������ʣ�000.0~999.9�ڣ�ǰ���0Ҳ�������䣩
<4> �������ʣ�0000.0~1851.8����/Сʱ��ǰ���0Ҳ�������䣩
<5> ģʽָʾ����NMEA0183 3.00�汾�����A=������λ��D=��֣�E=���㣬N=������Ч��

*/


/* ʵ���人���� GPS����
$GPGGA,064518.046,,,,,0,00,,,M,0.0,M,,0000*5A
$GPGLL,,,,,064518.046,V,N*76
$GPGSA,A,1,,,,,,,,,,,,,,,*1E
$GPGSV,3,1,12,18,56,351,,22,51,026,,14,51,206,21,19,48,285,*78
$GPGSV,3,2,12,26,38,041,,24,37,323,,03,37,281,,09,31,097,*78
$GPGSV,3,3,12,21,17,122,,25,13,176,,31,13,054,,20,00,266,*7A
$GPRMC,064518.046,V,,,,,,,250213,,,N*46
$GPVTG,,T,,M,,N,,K,N*2C

//�̵���
$GPGGA,161037.000,3030.6548,N,11402.4568,E,1,04,5.2,51.1,M,-15.5,M,,0000*42
$GPGSA,A,3,05,12,02,25,,,,,,,,,6.0,5.2,2.9*3B
$GPGSV,3,1,10,02,49,314,31,05,37,225,41,12,33,291,32,25,09,318,33*7C
$GPGSV,3,2,10,10,85,027,18,04,57,019,18,17,45,123,20,13,26,075,*7F
$GPGSV,3,3,10,23,14,050,23,40,18,253,33*71
$GPRMC,161037.000,A,3030.6548,N,11402.4568,E,0.00,,010613,,,A*71
$GPVTG,,T,,M,0.00,N,0.0,K,A*13

//��2��
$GPGGA,165538.000,3030.6519,N,11402.4480,E,2,05,1.9,39.5,M,-15.5,M,6.8,0000*68
$GPGSA,A,3,26,05,25,12,02,,,,,,,,2.7,1.9,2.0*3A
$GPGSV,3,1,11,10,63,029,18,02,58,344,23,05,55,247,46,04,50,053,26*75
$GPGSV,3,2,11,12,31,265,39,17,27,139,22,13,22,053,23,25,17,301,37*78
$GPGSV,3,3,11,26,11,180,43,23,04,036,,40,18,253,33*4A
$GPRMC,165538.000,A,3030.6519,N,11402.4480,E,0.00,71.87,010613,,,D*5E
$GPVTG,71.87,T,,M,0.00,N,0.0,K,D*31
$GPGGA,165539.000,3030.6519,N,11402.4480,E,2,05,1.9,39.5,M,-15.5,M,7.8,0000*68
$GPRMC,

�ȷ��� ����: 30�� 30�� 65

3030.6519 = 30�� + 30.6519�֣� 60���ƣ� 
30.6519 ��  --> 30.6519 / 60 = 0.510865�ȡ�  30.510865��

*/

/***************************** ���������� www.armfly.com (END OF FILE) *********************************/
